function tg1(x)
use my_prec
implicit none
real(mp) sin2, tg1, x
tg1=1.23_mp/((sqrt(sin(x)*sin(x)+1.23_mp*cos(x))+sin(x)))
return
end function