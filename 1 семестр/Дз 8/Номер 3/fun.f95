module fun
implicit none
contains

function tg0(x)
implicit none
real(4) tg0, tg, tg2, x
tg=tan(x)
tg2=tg*tg
tg0=sqrt(tg2+1.23/cos(x))-tg
return
end function

function dtg0(x)
implicit none
real(8) dtg0, tg, tg2, x
tg=tan(x)
tg2=tg*tg
dtg0=sqrt(tg2+1.23_8/cos(x))-tg
return
end function


function tg1(x)
implicit none
real(4) sin2, tg1, x
tg1=1.23/((sqrt(sin(x)*sin(x)+1.23*cos(x))+sin(x)))
return
end function

function dtg1(x)
implicit none
real(8) sin2, dtg1, x
dtg1=1.23_8/((sqrt(sin(x)*sin(x)+1.23_8*cos(x))+sin(x)))
return
end function

end module fun
